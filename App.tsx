import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import ScreenOne from './screens/screenOne';
import ScreenTwo from './screens/screenTwo';
import { Provider } from './utils/appProvider';


export type RootStackParamList = {
  Screen1: undefined;
  Screen2: { position: string; index: number, bench?: true };
};

const Stack = createStackNavigator<RootStackParamList>();


export default function App() {
  return (
    <Provider>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="Screen1" component={ScreenOne} options={{ title: 'Draft Your Squad' }} />
          <Stack.Screen name="Screen2" component={ScreenTwo} options={{ title: 'Add Player' }} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}