import React from 'react'
import {View, Text, Pressable} from 'react-native'
import BadgeIcon from './BadgeIcon';
import { useNavigation } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import { RootStackParamList } from '../App';

type Screen1NavigationProp = StackNavigationProp<
  RootStackParamList,
  'Screen1'
>;

const BenchPlayer = ({player, color, index}) => {
  const navigation = useNavigation<Screen1NavigationProp>();

  return (
    <Pressable style={{alignItems: 'center'}} onPress={() => navigation.navigate('Screen2', {position: '', index: index, bench: true})}>
      <BadgeIcon iconName={'tshirt'} count={player.number || '+'} color={color} />
      <View style={{padding: 10, width: '100%', margin: 6, borderTopLeftRadius: 15}}>
        <Text style={{borderColor: 'red', color: '#000', textAlign: 'center', padding: 3, fontSize: 12}}>
          {player.name || '-'}
        </Text>
        <Text style={{ textAlign: 'center', padding: 3, fontSize: 10, fontWeight: '600'}}>
          {player.club ? `${player.club}-${player.position}` : '-'}
        </Text>
      </View>
    </Pressable>
  )
}


export default BenchPlayer;