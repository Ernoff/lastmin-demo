import React from 'react';
import {View, Text, StyleSheet } from 'react-native';


const Budget = ({amount}) => {
  return (
    <View style={styles.container}>
        <View style={styles.line}/>
        <Text style={styles.text}>Budget: ${amount.toFixed(0)}</Text> 
        <View style={styles.line}/>
    </View>
  );
};

export default Budget;

const styles = StyleSheet.create({
  container: {flexDirection: 'row', marginLeft: '7%', marginTop: '3%'},
  line: {borderBottomColor: '#FFFEFF', borderWidth: 1, width: '25%', height: 0, alignSelf: 'center'},
  text: {textAlign: 'center', marginHorizontal: 10}
})