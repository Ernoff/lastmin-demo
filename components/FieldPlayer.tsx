import React from 'react'
import {View, Text, Pressable} from 'react-native'
import BadgeIcon from './BadgeIcon';
import { useNavigation } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import { RootStackParamList } from '../App';

type Screen1NavigationProp = StackNavigationProp<
  RootStackParamList,
  'Screen1'
>;

const FieldPlayer = ({player, position, index}) => {
  const navigation = useNavigation<Screen1NavigationProp>();

  return (
    <Pressable style={{alignItems: 'center'}} 
      onPress={() => navigation.navigate('Screen2', {position: position, index: index})}
    >
      <BadgeIcon iconName={'tshirt'} count={player.number || '+'} color={'#fff'} />
      {player.name && <View style={{padding: 10, width: '100%', margin: 6, borderTopLeftRadius: 15}}>
        <Text style={{backgroundColor: '#000', borderColor: 'red', color: '#fff', textAlign: 'center', padding: 3, fontSize: 12}}>
          {player.name}
        </Text>
        <Text style={{backgroundColor: '#fff', textAlign: 'center', padding: 3, fontSize: 10, fontWeight: '600'}}>
          {player.club}
        </Text>
      </View>}
    </Pressable>
  )
}


export default FieldPlayer;