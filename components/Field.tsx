import React from 'react';
import { View, StyleSheet, ImageBackground, Text } from 'react-native';
import field from '../assets/field.png';
import FieldPlayer from './FieldPlayer';

const Field = ({fieldPlayers}) => {
  return (
    <View style={styles.pitchContainer}>
      <ImageBackground source={field} resizeMode="stretch" style={{
        width: "100%",
        aspectRatio: 0.9,
        justifyContent: "space-around",
        alignItems: "center",
        transform: [{ scaleX: 1 }, {translateY: 35}, {perspective: 1} ],
      }}>
        {Object.keys(fieldPlayers).map((position) => (
        <View style={{
          flexDirection: "row",
          justifyContent: "space-around",
          width: "100%"
        }}>
          {fieldPlayers[position].map((player, index) => (
            <FieldPlayer player={player} position={position} index={index} key={player.key} />
          ))}
        </View>))}
      </ImageBackground>
    </View>
  );
};

const styles = StyleSheet.create({
  pitchContainer: {
    flex: 0.6,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Field;
