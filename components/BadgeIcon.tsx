import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import {FontAwesome5} from '@expo/vector-icons';

const BadgeIcon = ({ iconName, count, color }) => {
  return (
    <View style={styles.container}>
      <FontAwesome5 name={iconName} size={30} color={color} />
      <View style={styles.textWrapper}>
        <Text style={color == '#fff' ? styles.text : styles.invertColor}>{count}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    position: 'relative',
  },
  textWrapper: {
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
  text: {
    color: '#000',
    fontSize: 10,
  },
  invertColor: {
    color: '#fff',
    fontSize: 10,
  },
});

export default BadgeIcon;
