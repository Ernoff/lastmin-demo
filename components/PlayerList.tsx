import React, { useContext } from 'react';
import { Context } from '../utils/appProvider';
import { useHooks } from '../utils/useHooks';
import Table from './Table';


const PlayerList = ({position, index, bench}) => {
  const {playerData} = useContext<any>(Context)
  return (
      <Table body={playerData} position={position} index={index} bench={bench} />
  );
};

export default PlayerList;
