import React, { useContext } from 'react';
import { View, Text, StyleSheet, Pressable, ScrollView } from 'react-native';
import {AntDesign} from '@expo/vector-icons';
import { useHooks } from '../utils/useHooks';
import { StackNavigationProp } from '@react-navigation/stack';
import { RootStackParamList } from '../App';
import { useNavigation } from '@react-navigation/native';
import { Context } from '../utils/appProvider';

type Screen2NavigationProp = StackNavigationProp<
  RootStackParamList,
  'Screen2'
>;


const Table = ({body, position, index, bench}) => {
  return (
    <View style={styles.container}>
       {/* HEADERS */}
      <View style={styles.headerRow}>
        <View style={{...styles.cell}}>
          {/* TODO: extract into a standalone component*/}
          <Text style={{...styles.playerText, fontWeight: 'bold'}}>Player</Text>
        </View>
        <View style={{...styles.cell, flexDirection: 'row', justifyContent: 'flex-end'}}>
          <Text style={{...styles.text, fontWeight: 'bold', alignSelf: 'flex-end'}}>Form</Text>
          <AntDesign name={'arrowdown'} size={16} color={"#000"} />
        </View>
        <View style={{...styles.cell, flexDirection: 'row', justifyContent: 'flex-end'}}>
          <Text style={{...styles.text, fontWeight: 'bold', alignSelf: 'flex-end'}}>Price</Text>
          <AntDesign name={'arrowup'} size={16} color={"#000"} />
        </View>
        <View style={{...styles.cell, flexDirection: 'row', justifyContent: 'flex-end'}}>
          <Text style={{...styles.text, fontWeight: 'bold', alignSelf: 'flex-end'}}>Goals</Text>
          <AntDesign name={'arrowup'} size={16} color={"#000"} />
        </View>
      </View>
      {/* TABLE BODY */}
      <ScrollView>
        {body.map(entry => <TableBody body={entry} position={position} index={index} key={entry.key} bench={bench} />)}
      </ScrollView>
    </View>
  );
};

const TableBody = ({body, position, index, bench = false}) => {
  const navigation = useNavigation<Screen2NavigationProp>();
  const {addFieldPlayer, addBenchPlayer} = useContext<any>(Context);
  return (
    <Pressable 
      style={{...styles.row, borderWidth: 1, marginBottom: 10, borderRadius: 5}} 
      onPress={async () => {
        bench ? await addBenchPlayer(body, index) : await addFieldPlayer(body, position, index);
        navigation.navigate('Screen1')
      }}
    >
      <View style={{...styles.cell}}>
        <Text style={{...styles.playerText, fontWeight: 'bold'}}>{body.name}</Text>
      </View>
      <View style={styles.cell}>
        <Text style={styles.text}>{body.form}</Text>
      </View>
      <View style={styles.cell}>
        <Text style={styles.text}>{`$${body.price}k`}</Text>
      </View>
      <View style={styles.cell}>
        <Text style={{...styles.text}}>{body.goals}</Text>
      </View>
    </Pressable>
  );
};


const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: '5%',
    // padding: 16,
    backgroundColor: '#F4F5F4',
    width: '100%'
  },
  headerRow: {
    flexDirection: 'row',
    borderColor: '#ddd',
    padding: 15,
  },
  row: {
    flexDirection: 'row',
    borderColor: '#ddd',
    padding: 15,
    backgroundColor: '#fff',
  },
  cell: {
    flex: 0.5,
  },
  playerText: {
    color: '#000',
  },
  text: {
    color: '#000',
    alignSelf: 'flex-end'
  },
});

export default Table;
