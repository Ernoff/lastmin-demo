import { LinearGradient } from 'expo-linear-gradient';
import React, { useContext, useEffect } from 'react';
import { SafeAreaView, StyleSheet, Text, View } from "react-native";
import {FontAwesome5} from '@expo/vector-icons';
import PlayerList from '../components/PlayerList';
import Budget from '../components/Budget';
import { useHooks } from '../utils/useHooks';
import { Context } from '../utils/appProvider';

export default function ScreenTwo({route}) {
  const {amount} = useContext<any>(Context)
  useEffect(() => {
    
  }, [amount]);
  return <SafeAreaView style={styles.container}>
     <View style={{ flex: 0.08, width: '90%', marginLeft: '5%', marginTop: '5%', flexDirection: 'row', justifyContent: 'space-between'}}>
        <LinearGradient
          colors={['#306B9C', '#48A59A']}
          style={styles.button}>
            <Text style={{...styles.text, fontSize: 8, paddingVertical: 4}}>Position</Text>
            <Text style={styles.text}>Forward</Text>
        </LinearGradient>
        <View
          style={{...styles.button, backgroundColor: '#48A59A'}}>
            <Text style={{...styles.text, fontSize: 8, paddingVertical: 4}}>Price</Text>
            <View style={{flexDirection: 'row'}}><Text style={styles.text}>$10 mil </Text><FontAwesome5 name={'caret-down'} size={18} color={"#fff"} /></View>
        </View>
        <LinearGradient
          colors={['#48A59A', '#306B9C']}
          style={styles.button}>
            <Text style={{...styles.text, fontSize: 8, paddingVertical: 4}}>Clubs</Text>
            {/* TODO: Common pattern detected, extract to component */}
            <View style={{flexDirection: 'row'}}><Text style={styles.text}>All Clubs </Text><FontAwesome5 name={'caret-down'} size={18} color={"#fff"} /></View>
        </LinearGradient>
     </View>
    <Budget amount={amount} />
    <PlayerList position={route.params.position} index={route.params.index} bench={route.params.bench} />
  </SafeAreaView>
}

const styles = StyleSheet.create({
  container: {
    flex: 1, 
    // alignItems: "flex-start",
    backgroundColor: '#F4F5F4',
  },
  button: {
    paddingHorizontal: 15,
    alignItems: 'center',
    borderRadius: 10,
    height: '100%',
    width: '30%'
  },
  text: {
    color: '#fff',
    fontWeight: '600'
  }
});