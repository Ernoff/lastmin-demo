import React, { useContext, useEffect } from 'react';
import { SafeAreaView, StyleSheet, Text, View } from "react-native";
import Budget from '../components/Budget';
import Field from "../components/Field";
import BenchPlayer from '../components/BenchPlayer';
import { useHooks } from '../utils/useHooks';
import { Context } from '../utils/appProvider';

export default function ScreenOne() {
  const {amount, benchPlayers, fieldPlayers} = useContext<any>(Context)
  useEffect(() => {
    
  }, [amount, fieldPlayers, benchPlayers]);
  return <SafeAreaView style={styles.container}>
     <View style={{backgroundColor: 'white', flex: 0.1, width: '100%', }}>
      <Text style={{color: '#4AA49B', fontSize: 22, fontWeight: '600', textAlign: 'center', paddingTop: '1%'}}>"FC Dream Squad"</Text>
      <Budget amount={amount} />
      <Text style={{ textAlign: 'center', paddingTop: '1%'}}>Tap on a player position in order to add a new player</Text>
     </View>
    <Field fieldPlayers={fieldPlayers}/>
    {/* Benched Players */}
      <View style={{backgroundColor: '#4CCE4D', flex: 0.3, width: '100%', marginTop: '13%'}}>
        <View style={{width: '80%', borderWidth: 1, alignSelf: 'center', marginTop: '3%', borderRadius: 10, paddingTop: '2%', backgroundColor: '#AAE1BE', borderColor: '#AAE1BE'  }}>
          <Text style={{marginHorizontal: '5%', textAlign: 'center', fontWeight: '600'}}> Benched Players</Text>
          <View style={{justifyContent: "space-around", flexDirection: 'row', alignSelf: 'center', marginTop: '5%' }}>
          {benchPlayers.map((player, index) => (
              <BenchPlayer player={player} color={'grey'} index={index} />
            ))}
            </View>
        </View>
      </View>
  </SafeAreaView>
}

const styles = StyleSheet.create({
  container: {
    flex: 1, 
    alignItems: "flex-start",
    backgroundColor: 'white',
  }
});