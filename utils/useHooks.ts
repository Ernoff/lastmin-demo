import { useEffect, useState } from "react"
import { StackNavigationProp } from '@react-navigation/stack';
import { RootStackParamList } from '../App';

export const useHooks = () => {

  const playerData = [
    {key: 1, name: 'Martineli', club: 'CHE', number: 6, position: 'FWD', form: '9.0', price: 65, goals: 23, amountinFull: 65000 },
    {key: 2, name: 'Martineli', club: 'CHE', number: 6, position: 'FWD', form: '9.0', price: 65, goals: 23, amountinFull: 65000 },
    {key: 3, name: 'Martineli', club: 'CHE', number: 6, position: 'FWD', form: '9.0', price: 65, goals: 23, amountinFull: 65000 },
    {key: 4, name: 'Martineli', club: 'CHE', number: 6, position: 'FWD', form: '9.0', price: 65, goals: 23, amountinFull: 65000 },
    {key: 5, name: 'Martineli', club: 'CHE', number: 6, position: 'FWD', form: '9.0', price: 65, goals: 23, amountinFull: 65000 },
    {key: 6, name: 'Martineli', club: 'CHE', number: 6, position: 'FWD', form: '9.0', price: 65, goals: 23, amountinFull: 65000 },
    {key: 7, name: 'Martineli', club: 'CHE', number: 6, position: 'FWD', form: '9.0', price: 65, goals: 23, amountinFull: 65000 },
    {key: 8, name: 'Martineli', club: 'CHE', number: 6, position: 'FWD', form: '9.0', price: 90, goals: 23, amountinFull: 90000 },
    {key: 9, name: 'Martineli', club: 'CHE', number: 6, position: 'FWD', form: '9.0', price: 65, goals: 23, amountinFull: 65000 },
    {key: 10, name: 'Martineli', club: 'CHE', number: 6, position: 'FWD', form: '9.0', price: 65, goals: 23, amountinFull: 65000 },
    {key: 11, name: 'Martineli', club: 'CHE', number: 6, position: 'FWD', form: '9.0', price: 65, goals: 23, amountinFull: 65000 },
    {key: 12, name: 'Martineli', club: 'CHE', number: 6, position: 'FWD', form: '9.0', price: 68, goals: 23, amountinFull: 68000 },
    {key: 13, name: 'Martineli', club: 'CHE', number: 6, position: 'FWD', form: '9.0', price: 65, goals: 23, amountinFull: 65000 },
    {key: 14, name: 'Martineli', club: 'CHE', number: 6, position: 'FWD', form: '9.0', price: 65, goals: 23, amountinFull: 65000 },
    {key: 15, name: 'Martineli', club: 'CHE', number: 6, position: 'FWD', form: '9.0', price: 68, goals: 23, amountinFull: 68000 },
  ]
  const players = {
    GKR: [{name: null, club: null, number: 0}],
    DEF: [{name: null, club: null, number: 0}, {name: null, club: null, number: 0}, {name: null, club: null, number: 0}, {name: null, club: null, number: 0}],
    MID: [{name: null, club: null, number: 0}, {name: null, club: null, number: 0}, {name: null, club: null, number: 0}],
    FWD: [{name: null, club: null, number: 0}, {name: null, club: null, number: 0}, {name: null, club: null, number: 0}],
  }

  const [fieldPlayers, setFieldPlayers] = useState<object>(players)
  const [benchPlayers, setBenchPlayers] = useState<object[]>([
    {name: null, club: null, number: 0, position: null},
    {name: null, club: null, number: 0, position: null},
    {name: null, club: null, number: 0, position: null},
    {name: null, club: null, number: 0, position: null},
  ])
  const [amount, setAmount] = useState<number>(1000000);

  const addFieldPlayer = async (player: any, position: string, index: number) => {
    // Ideally, if player swap, 
    // remove old player and add his value to total before eveluating
    const reducedAmount = amount - player.amountinFull;
    if (reducedAmount < 0) {
      alert('insufficient funds');
      return;
    }
    fieldPlayers[position][index] = player;
    setFieldPlayers(fieldPlayers)
    setAmount(reducedAmount);
  }

  const addBenchPlayer = (player: any, index: number) => {
    const reducedAmount = amount - player.amountinFull
    if (reducedAmount < 0) {
      alert('insufficient funds');
      return;
    }
    benchPlayers[index] = player;
    setBenchPlayers(benchPlayers)
    setAmount(reducedAmount);
  }

  return {
    benchPlayers,
    fieldPlayers,
    playerData,
    amount,
    setAmount,
    addFieldPlayer,
    addBenchPlayer
  }
}