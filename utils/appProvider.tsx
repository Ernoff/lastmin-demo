import React, { createContext } from 'react';
import { useHooks } from './useHooks';

// Create a context
export const Context = createContext({});

// Create a context provider component
export const Provider = ({ children }) => {
  const {fieldPlayers, addFieldPlayer, benchPlayers, addBenchPlayer, amount, playerData } = useHooks();

  return (
    <Context.Provider value={{ fieldPlayers, addFieldPlayer, benchPlayers, amount, playerData, addBenchPlayer }}>
      {children}
    </Context.Provider>
  );
};
